package schema

type AuthDetails struct {
	UUID   string
	UserId uint64
}
