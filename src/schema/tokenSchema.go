package schema

// import "github.com/dgrijalva/jwt-go"

// type TokenClaims struct {
// 	UserID     uint64 `json:"user_id"`
// 	Authorized bool   `json:"is_authorized"`
// 	UUID       string
// 	jwt.StandardClaims
// }

type TokenType int

const (
	unknown TokenType = iota
	AccessTokenType
	RefreshTokenType
	sentinel
)

type TokenDetails struct {
	// AccessToken  string
	// RefreshToken string
	AccessUuid  string
	RefreshUuid string
	AtExpires   int64 // Access Token Exp
	RtExpires   int64 // Refresh Token Exp
}

type Tokens struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}
