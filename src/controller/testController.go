package controller

import (
	"gin-playgrond/jwt-adv-demo/src/schema"
	"gin-playgrond/jwt-adv-demo/src/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

type TestController interface {
	Ping(ctx *gin.Context)
}

type testController struct {
	authController AuthController
	redisService   service.RedisService
}

func NewTestController(auth AuthController, redis service.RedisService) TestController {
	return &testController{
		authController: auth,
		redisService:   redis,
	}
}

func (c *testController) Ping(ctx *gin.Context) {
	token := c.authController.ExtractToken(ctx.Request)
	tokenAuth, err := c.authController.ExtractTokenMetadata(token, schema.AccessTokenType)
	if err != nil {
		ctx.JSON(http.StatusUnauthorized, "unauthorized extract")
		return
	}
	userId, err := c.redisService.ReadAccessAuth(tokenAuth)
	if err != nil {
		ctx.JSON(http.StatusUnauthorized, "unauthorized read")
		return
	}
	ctx.JSON(http.StatusCreated, gin.H{
		"status": "OK",
		"uid":    userId,
	})
}
