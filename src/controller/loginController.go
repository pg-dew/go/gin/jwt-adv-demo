package controller

// token controller

import (
	"fmt"
	"gin-playgrond/jwt-adv-demo/src/schema"
	"gin-playgrond/jwt-adv-demo/src/service"
	"net/http"

	"github.com/gin-gonic/gin"
)

type User struct {
	ID       uint64 `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
}

//A sample use
var user = User{
	ID:       1,
	Username: "admin",
	Password: "yeeee",
}

type LoginController interface {
	Login(ctx *gin.Context)
	Logout(ctx *gin.Context)
	Refresh(ctx *gin.Context)
}

type loginController struct {
	loginService   service.LoginService
	redisService   service.RedisService
	authController AuthController
}

func NewLoginController(loginService service.LoginService, redisService service.RedisService, authController AuthController) LoginController {
	return &loginController{
		loginService:   loginService,
		redisService:   redisService,
		authController: authController,
	}
}

func (c *loginController) Login(ctx *gin.Context) {
	var u User

	tokens := &schema.Tokens{}

	if err := ctx.ShouldBindJSON(&u); err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, "Invalid JSON provided")
		return
	}
	//compare the user from the request, with the one we defined:
	if user.Username != u.Username || user.Password != u.Password {
		ctx.JSON(http.StatusUnauthorized, "Invalid login credentials")
		return
	}

	if err := c.loginService.CreateTokens(user.ID, tokens); err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, tokens)
}

type RefreshBody struct {
	RefreshToken string `json:"refresh_token"`
}

func (c *loginController) Logout(ctx *gin.Context) {
	token := c.authController.ExtractToken(ctx.Request)
	accessAuth, err := c.authController.ExtractTokenMetadata(token, schema.AccessTokenType)

	if err != nil {
		ctx.JSON(http.StatusUnauthorized, "Unauthorized")
		return
	}

	var body RefreshBody
	if err := ctx.ShouldBindJSON(&body); err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, "Invalid JSON body provided")
		return
	}

	refreshAuth, err := c.authController.ExtractTokenMetadata(body.RefreshToken, schema.RefreshTokenType)
	if err != nil {
		ctx.JSON(http.StatusUnauthorized, "Invalid Refresh Token"+err.Error())
		return
	}

	td := &schema.TokenDetails{
		AccessUuid:  accessAuth.UUID,
		RefreshUuid: refreshAuth.UUID,
	}

	if deleted, delErr := c.redisService.DeleteAllAuth(td); delErr != nil || deleted == 0 {
		ctx.JSON(http.StatusUnauthorized, "Unauthorized redis")
		return
	}
	ctx.JSON(http.StatusOK, "Logged out")
}

func (c *loginController) Refresh(ctx *gin.Context) {

	var body schema.Tokens
	if err := ctx.ShouldBindJSON(&body); err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, err.Error())
		return
	}

	// Verify refresh token
	if err := c.authController.TokenValid(body.RefreshToken, schema.RefreshTokenType); err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, err.Error())
		return
	}

	refreshAuth, err := c.authController.ExtractTokenMetadata(body.RefreshToken, schema.RefreshTokenType)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "Invalid Refresh Token "+err.Error())
		return
	}

	// Redis: Delete prev. refresh auth
	if deleted, err := c.redisService.DeleteAuth(refreshAuth); err != nil || deleted == 0 {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "Unauthorized redis")
		return
	}

	// Check uid match
	accessAuth, err := c.authController.ExtractTokenMetadata(body.AccessToken, schema.AccessTokenType)
	if err != nil && err.Error() != "Token is expired" {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "Invalid access token "+err.Error())
		return
	}

	if accessAuth.UserId != refreshAuth.UserId {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, "Invalid user")
		return
	}

	// Redis: Delete prev. access auth
	if deleted, err := c.redisService.DeleteAuth(accessAuth); err != nil || deleted == 0 {
		fmt.Println("Redis: Skip access delete")
	}

	// Create new pairs of refresh and access tokens
	tokens := &schema.Tokens{}
	if err := c.loginService.CreateTokens(user.ID, tokens); err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, err.Error())
		return
	}

	ctx.JSON(http.StatusOK, tokens)
}
