package controller

import (
	"fmt"
	"gin-playgrond/jwt-adv-demo/src/schema"
	"net/http"
	"strconv"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

type AuthController interface {
	ExtractToken(r *http.Request) string
	TokenValid(tokenString string, tokenType schema.TokenType) (err error)
	ExtractTokenMetadata(tokenString string, tokenType schema.TokenType) (*schema.AuthDetails, error)
}

type authController struct {
}

func NewAuthController() AuthController {
	return &authController{}
}

func (c *authController) TokenValid(tokenString string, tokenType schema.TokenType) error {
	// var token jwt.Token

	token, err := c.verifyToken(tokenString, tokenType)
	if err != nil {
		return err
	}

	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		return err
	}
	return nil
}

func (c *authController) verifyToken(tokenString string, tokenType schema.TokenType) (token *jwt.Token, err error) {
	// tokenString := c.extractToken(r)

	token, err = jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		//Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		if tokenType == schema.AccessTokenType {
			// return []byte(os.Getenv("ACCESS_SECRET")), nil
			return []byte("jdnfksdmfksd"), nil //this should be in an env file
		} else if tokenType == schema.RefreshTokenType {
			// return []byte(os.Getenv("REFRESH_SECRET")), nil
			return []byte("mcmvmkmsdnfsdmfdsjf"), nil //this should be in an env file
		} else {
			return nil, nil
		}
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}

func (c *authController) ExtractToken(r *http.Request) string {
	bearToken := r.Header.Get("Authorization")
	//normally Authorization the_token_xxx
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}

func (c *authController) ExtractTokenMetadata(tokenString string, tokenType schema.TokenType) (*schema.AuthDetails, error) {
	// var token jwt.Token
	// err := c.verifyToken(r, &token)

	token, err := c.verifyToken(tokenString, tokenType)
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		var tokenUuid string
		if tokenType == schema.RefreshTokenType {
			tokenUuid, ok = claims["refresh-UUID"].(string)
		} else {
			tokenUuid, ok = claims["access-UUID"].(string)
		}
		if !ok {
			return nil, err
		}
		userId, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["user_id"]), 10, 64)
		if err != nil {
			return nil, err
		}
		return &schema.AuthDetails{
			UUID:   tokenUuid,
			UserId: userId,
		}, nil
	}
	return nil, err
}
