package service

import (
	"context"
	"gin-playgrond/jwt-adv-demo/src/schema"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
)

type RedisService interface {
	TestConnection() (err error)
	SaveAuth(userID uint64, td *schema.TokenDetails) (err error)
	ReadAccessAuth(detail *schema.AuthDetails) (uint64, error)
	DeleteAuth(detail *schema.AuthDetails) (deleted int64, err error)
	DeleteAllAuth(td *schema.TokenDetails) (deleted int64, err error)
}

type redisService struct {
	client *redis.Client
	rctx   context.Context
}

func NewRedisService(redisCtx context.Context) RedisService {
	//Initializing redis
	dsn := os.Getenv("REDIS_DSN")
	if len(dsn) == 0 {
		dsn = "localhost:6379"
	}

	return &redisService{
		client: redis.NewClient(&redis.Options{
			Addr:     dsn, //redis port
			Password: "",  // no password set
			DB:       0,   // use default DB
		}),
		rctx: redisCtx,
	}
}

func (s *redisService) TestConnection() (err error) {
	_, err = s.client.Ping(s.rctx).Result()
	return // return err
}

// Change to blacklist access token instead of whitelisting
func (s *redisService) SaveAuth(userID uint64, td *schema.TokenDetails) (err error) {
	accessExp := time.Unix(td.AtExpires, 0) //converting Unix to UTC(to Time object)
	refreshExp := time.Unix(td.RtExpires, 0)
	now := time.Now()

	if err = s.client.Set(s.rctx, td.AccessUuid, userID, accessExp.Sub(now)).Err(); err != nil {
		return
	}

	if err = s.client.Set(s.rctx, td.RefreshUuid, userID, refreshExp.Sub(now)).Err(); err != nil {
		return
	}
	return
}

func (s *redisService) ReadAccessAuth(detail *schema.AuthDetails) (uint64, error) {
	redisVal, err := s.client.Get(s.rctx, detail.UUID).Result()
	if err != nil {
		return 0, err
	}
	userID, _ := strconv.ParseUint(redisVal, 10, 64)
	return userID, nil
}

func (s *redisService) DeleteAuth(detail *schema.AuthDetails) (deleted int64, err error) {
	deleted, err = s.client.Del(s.rctx, detail.UUID).Result()
	if err != nil {
		return 0, err
	}
	return
}

func (s *redisService) DeleteAllAuth(td *schema.TokenDetails) (deleted int64, err error) {
	// remove token
	deleted, err = s.client.Del(s.rctx, td.AccessUuid, td.RefreshUuid).Result()
	if err != nil {
		return 0, err
	}
	return
}
