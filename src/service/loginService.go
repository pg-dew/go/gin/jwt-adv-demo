package service

import (
	"gin-playgrond/jwt-adv-demo/src/schema"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/twinj/uuid"
)

type accessTokenClaims struct {
	UserID     uint64 `json:"user_id"`
	Authorized bool   `json:"is_authorized"`
	AccessUUID string `json:"access-UUID"`
	jwt.StandardClaims
}

type refreshTokenClaims struct {
	UserID      uint64 `json:"user_id"`
	Authorized  bool   `json:"is_authorized"`
	RefreshUUID string `json:"refresh-UUID"`
	jwt.StandardClaims
}

// type tokenDetails struct {
// 	// AccessToken  string
// 	// RefreshToken string
// 	AccessUuid  string
// 	RefreshUuid string
// 	AtExpires   int64
// 	RtExpires   int64
// }

type LoginService interface {
	CreateTokens(userid uint64, tokens *schema.Tokens) (err error)
}

type loginService struct {
	redisService RedisService
}

func NewLoginService(redisService RedisService) LoginService {
	return &loginService{
		redisService: redisService,
	}
}

func (s *loginService) CreateTokens(userID uint64, tokens *schema.Tokens) (err error) {

	td := &schema.TokenDetails{
		AtExpires:   time.Now().Add(time.Minute * 1).Unix(),
		AccessUuid:  uuid.NewV4().String(),
		RtExpires:   time.Now().Add(time.Hour * 1).Unix(),
		RefreshUuid: uuid.NewV4().String(),
	}

	//Creating Access Token
	os.Setenv("ACCESS_SECRET", "jdnfksdmfksd") //this should be in an env file
	// atClaims := jwt.MapClaims{}
	// atClaims["authorized"] = true
	// atClaims["user_id"] = userid
	// atClaims["exp"] = time.Now().Add(time.Minute * 15).Unix()

	stdClaims := jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Minute * 15).Unix(),
		// Issuer:    service.issuer,
		Issuer:   "me",
		IssuedAt: time.Now().Unix(),
	}

	atClaims := &accessTokenClaims{
		UserID:         userID,
		Authorized:     true,
		AccessUUID:     td.AccessUuid,
		StandardClaims: stdClaims,
	}

	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)

	// fmt.Println("User ID ", userID)
	tokens.AccessToken, err = at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))

	if err != nil {
		return //return err
	}

	//Creating Refresh Token
	os.Setenv("REFRESH_SECRET", "mcmvmkmsdnfsdmfdsjf") //this should be in an env file
	// rtClaims := jwt.MapClaims{}
	// rtClaims["refresh_uuid"] = td.RefreshUuid
	// rtClaims["user_id"] = userID
	// rtClaims["exp"] = td.RtExpires

	stdClaims = jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * 24 * 7).Unix(),
		Issuer:    "me",
		IssuedAt:  time.Now().Unix(),
	}

	rtClaims := &refreshTokenClaims{
		UserID:         userID,
		Authorized:     true,
		RefreshUUID:    td.RefreshUuid,
		StandardClaims: stdClaims,
	}

	rt := jwt.NewWithClaims(jwt.SigningMethodHS256, rtClaims)

	tokens.RefreshToken, err = rt.SignedString([]byte(os.Getenv("REFRESH_SECRET")))
	if err != nil {
		return
	}

	if err = s.redisService.SaveAuth(userID, td); err != nil {
		return
	}

	return
}

// func (s *loginService) VerifyRefreshToken(tokenString string) (token *jwt.Token, err error) {
// 	os.Setenv("REFRESH_SECRET", "mcmvmkmsdnfsdmfdsjf") //this should be in an env file
// 	token, err = jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
// 		//Make sure that the token method conform to "SigningMethodHMAC"
// 		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
// 			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
// 		}
// 		return []byte(os.Getenv("REFRESH_SECRET")), nil
// 	})
// 	return
// }
