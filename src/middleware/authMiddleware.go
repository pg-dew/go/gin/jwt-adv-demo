package middleware

import (
	"gin-playgrond/jwt-adv-demo/src/controller"
	"gin-playgrond/jwt-adv-demo/src/schema"
	"net/http"

	"github.com/gin-gonic/gin"
)

type AuthMiddleware interface {
	TokenAccessAuth() gin.HandlerFunc
}

type authMiddleware struct {
	authController controller.AuthController
}

func NewAuthMiddleware(authController controller.AuthController) AuthMiddleware {
	return &authMiddleware{
		authController: authController,
	}
}

func (m *authMiddleware) TokenAccessAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := m.authController.ExtractToken(c.Request)
		err := m.authController.TokenValid(token, schema.AccessTokenType)
		if err != nil {
			c.JSON(http.StatusUnauthorized, err.Error())
			// c.Abort()
			// return
		}
		c.Next()
	}
}
