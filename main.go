package main

import (
	"context"
	"gin-playgrond/jwt-adv-demo/src/controller"
	"gin-playgrond/jwt-adv-demo/src/middleware"
	"gin-playgrond/jwt-adv-demo/src/service"
	"log"

	"github.com/gin-gonic/gin"
)

var (
	router = gin.Default()

	redisService    service.RedisService       = service.NewRedisService(context.Background())
	loginService    service.LoginService       = service.NewLoginService(redisService)
	loginController controller.LoginController = controller.NewLoginController(loginService, redisService, authController)
	authController  controller.AuthController  = controller.NewAuthController()
	authMiddleware  middleware.AuthMiddleware  = middleware.NewAuthMiddleware(authController)
	testController  controller.TestController  = controller.NewTestController(authController, redisService)
)

func main() {
	if err := redisService.TestConnection(); err != nil {
		panic(err)
	}
	router.POST("/login", loginController.Login)
	router.POST("/test", authMiddleware.TokenAccessAuth(), testController.Ping)
	router.POST("/logout", authMiddleware.TokenAccessAuth(), loginController.Logout)
	router.POST("/refresh", loginController.Refresh)
	log.Fatal(router.Run(":4004"))
}
