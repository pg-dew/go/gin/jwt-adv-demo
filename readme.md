# JWT Auth with no loophole

- Using a persistence storage layer (Redis) to store JWT metadata. This will enable us to invalidate a JWT the very second a the user logs out, thereby improving security.
- Using the concept of a refresh token to generate a new access token, in the event that the access token expired, thereby improving the user experience.

> Adapted from https://learn.vonage.com/blog/2020/03/13/using-jwt-for-authentication-in-a-golang-application-dr/
